PATH=$PATH:~/$HOME/.local/bin
export VISUAL=nano export EDITOR="$VISUAL"

##Minhas configurações do Bash##
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples



export PS1='\$ '

# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi


alias ls='ls --color=auto'
alias grep='grep --color=auto'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Configuração do mpg123 'Player no terminal'

alias mpg='mpg123 --continue -v -E ~/.mpg_equal.txt --stereo --float --rva-mix --control --title --long-tag *mp3'

alias mpgr='find . -iname "*.mp3" | mpg123 -E ~/.mpg_equal.txt --stereo --float --rva-album --rva-mix --title --fuzzy --long-tag -Z --list -'

alias mps='mpg123 --stereo --float  --control -v -v -@'



alias Musicas='cd ~/Música'
alias Baile='cd ~/Música/Baile'
alias AulaparaMulheres='cd ~/Música/Bolero'
alias ChaChaCha='cd ~/Música/Cha Cha Cha'
alias Chorinho='cd ~/Música/Chorinho'
alias Fabio='cd ~/Música/Fabio'
alias Ferroviaria='cd ~/Música/Ferroviaria'
alias Forrolento='cd ~/Música/Forró lento'
alias Forromedio='cd ~/Música/Forró médio'
alias Forrorapido='cd ~/Música/Forró rápido'
alias Fox='cd ~/Música/Fox'
alias Media='cd ~/Música/Media'
alias RedutodoSamba='cd ~/Música/Reduto do Samba'
alias Samba='cd ~/Música/Samba'
alias Sertanejo='cd ~/Música/Sertanejo'
alias Show='cd ~/Música/Show'
alias Tango='cd ~/Música/Tango'
alias Valsa='cd ~/Música/Valsa'
alias Zouk='cd ~/Música/Zouk'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
